﻿using Client;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace TCP_SERVER
{
    class Program
    {
        static int port = 8005; // порт для приема входящих запросов
        static void Main(string[] args)
        {
           
                // получаем адреса для запуска сокета
                IPEndPoint ipPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), port);

            // создаем сокет
            Socket listenSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                // связываем сокет с локальной точкой, по которой будем принимать данные
                listenSocket.Bind(ipPoint);

                // начинаем прослушивание
                listenSocket.Listen(10);

                Console.WriteLine("Сервер запущен. Ожидание подключений...");

                while (true)
                {
                    Socket handler = listenSocket.Accept();
                    // получаем сообщение
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0; // количество полученных байтов
                    byte[] data = new byte[256]; // буфер для получаемых данных

                    do
                    {
                        bytes = handler.Receive(data);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (handler.Available > 0);
                    var stinger = builder.ToString().Split("\\");
                    switch (stinger[0])
                    {
                        case "Get":
                            using (ApplicationContext db = new ApplicationContext())
                            {
                                customer user1 = new customer { login = stinger[1], password = stinger[2] };
                                bancaccount user2 = new bancaccount { Nomer = stinger[3], CVV = stinger[4] };

                                db.customers.Add(user1);
                                db.SaveChanges();
                                db.bancaccount.Add(user2);
                                db.SaveChanges();
                            }
                            break;
                    }

                    // отправляем ответ
                    string message = "OKEY";
                    data = Encoding.Unicode.GetBytes(message);
                    handler.Send(data);
                    // закрываем сокет
                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
