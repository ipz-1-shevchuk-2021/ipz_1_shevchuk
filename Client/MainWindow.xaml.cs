﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            foreach (UIElement el in MainRoot.Children)
            {
                if (el is Button button)
                {
                    button.Click += Button_Click;
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (PinLable.Text.Length <4)
            {
                PinLable.Text += "*";
            }
        }

        private void Button_PropositionWindow_Click(object sender, RoutedEventArgs e)
        {
            ProposWindow proposition = new ProposWindow();
            proposition.Show();
            Close();
        }

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Button_Back_Click(object sender, RoutedEventArgs e)
        {
            //PinLable.Text
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }
    }
}
