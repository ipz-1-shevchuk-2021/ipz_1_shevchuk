﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Логика взаимодействия для ServiceWindow.xaml
    /// </summary>
    public partial class ServiceWindow : Window
    {
        public ServiceWindow()
        {
            InitializeComponent();
        }

        private void Button_BalanceWindow_Click(object sender, RoutedEventArgs e)
        {
            BalanceWindow balanceWindow = new BalanceWindow();
            balanceWindow.Show();
            Close();
        }

        private void Button_NumpedWindow_Click(object sender, RoutedEventArgs e)
        {
            NumpedWindow numpedWindow = new NumpedWindow();
            numpedWindow.Show();
            Close();
        }

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Button_ChangePinWindow_Click(object sender, RoutedEventArgs e)
        {
            ChangePinWindow pinWindow = new ChangePinWindow();
            pinWindow.Show();
            Close();
        }

        private void Button_RuplenishWindow_Click(object sender, RoutedEventArgs e)
        {
            ReplenishWindow replenishWindow = new ReplenishWindow();
            replenishWindow.Show();
            Close();
        }
    }
}
