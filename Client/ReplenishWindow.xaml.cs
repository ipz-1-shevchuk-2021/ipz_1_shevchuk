﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client
{
    /// <summary>
    /// Логика взаимодействия для ReplenishWindow.xaml
    /// </summary>
    public partial class ReplenishWindow : Window
    {
        public ReplenishWindow()
        {
            InitializeComponent();
        }

        private void Button_ServiceWindow__Click(object sender, RoutedEventArgs e)
        {
            ServiceWindow serviceWindow = new ServiceWindow();
            serviceWindow.Show();
            Close();
        }

        private void Button_NumpedWindow__Click(object sender, RoutedEventArgs e)
        {
            NumpedWindow numped = new NumpedWindow();
            numped.Show();
            Close();
        }
    }
}
